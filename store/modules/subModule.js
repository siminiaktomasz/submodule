export const subModule = {
    namespaced: true,

    strict: process.env.NODE_ENV !== "production",

    state: {
        text: 'SubModule message!'
    },
}